# 《沉浸式剖析OpenHarmony源代码》简介

<img src="figures/cover.jpg" alt="cover" style="zoom: 50%;" />

## 关于作者

**梁开祝**，毕业于四川大学计算机学院计算机科学与技术专业，擅长Linux底层驱动开发、音视频处理芯片驱动开发、HDMI/HDCP领域的驱动开发等技术，曾在OPPO公司蓝光事业部担任高级驱动工程师，并全程参与了OPPO全系列蓝光播放器的研发工作。后来因为工作的调整，在内部转岗到手机研发部门，负责手机摄像头的驱动开发工作。再后因个人原因离开OPPO，在了解到OpenHarmony开源后，被深深吸引而转入OpenHarmony的底层开发，在学习OpenHarmony开发技术过程中总结和整理成本书。



## 前言

自2019年以来，由中美脱钩引发了科技圈内的一系列重大事件，在网络上引发了大量的相关讨论，以至后来的HarmonyOS发布和OpenHarmony 1.0开源，我也只是当做行业新闻来看而已。直到2020年初，我在头条上也看到了业内知名大佬对HarmonyOS的一些科普视频，以及对所谓“HarmonyOS套壳Android”相关言论的驳斥之后，我才开始有意识地尝试了解HarmonyOS和OpenHarmony。我开始在OpenHarmony技术社区上学习OpenHarmony的知识，阅读OpenHarmony的源代码，并购买开发板对OpenHarmony的知识要点进行验证，期间也做了大量的学习笔记和总结。

在对OpenHarmony有一定的理解之后，我确定了自己未来的发展方向，因为我对OpenHarmony的发展前景深信不疑，这是IT行业十年一遇的机会，过去十余年里我曾经三次错过Android，如今我不想再错过OpenHarmony！所以我一边全力学习OpenHarmony开发技术，一边思考如何抓住这次机会。

在2021年6月，我基于发布不久的OpenHarmony 1.1.0 LTS(2021-04-01)版本代码，深度分析了分布式任务调度子系统的samgr_lite组件，并在OpenHarmony技术社区发表了一系列与之相关的技术文章（即本书第7章内容的原型）。之后，我盘点了一下自己在这段时间里整理的文档资料、学习总结以及相关的表格、流程图等，发现内容虽然零散不成体系，但是篇幅已颇具规模，而且已经开始触及OpenHarmony的一些技术要点。

此时，OpenHarmony相关的学习资料，主要还以官方文档和社区博文为主，而技术图书，特别是与系统开发或设备驱动开发相关的图书基本还没有。后来陆续出版的OpenHarmony图书，也以应用开发为主。所以，我萌生了“把自己整理的OpenHarmony学习笔记整理成书”的想法，也把实现这个想法当作自己抓住OpenHarmony这次机会的一次尝试。有了这个想法之后，我开始站在图书编排写作的角度来学习总结OpenHarmony技术，并进行整理和汇总，最终形成了读者现在所看到的这本书。



## 本书的组织结构

本书的章节安排，基本上就是本人学习OpenHarmony开发从入门到深入的过程实录，从一个初学者的视角来一边学习，一边总结，渐进式地进行条分缕析，以展现我所理解的OpenHarmony。

本书分为9章，各章介绍的主要内容如下。

- 第1章，“系统简介”：简单介绍OpenHarmony的发展历史、技术特性和发展前景。

- 第2章，“搭建开发环境”：由于OpenHarmony设备开发环境的搭建步骤相当繁琐，很多开发者在入门OpenHarmony驱动开发时都对此颇感头痛，因此本章提供了一个清晰的开发环境搭建步骤，帮助开发者扫清入门的障碍。

- 第3章，“系统架构”：简单介绍OpenHarmony的系统架构和一二级目录结构，旨在让开发者对OpenHarmony有一个整体上的初步认识。

- 第4章，“构建子系统”：OpenHarmony的编译构建体系非常复杂，而且在编译构建时常常会交叉使用多种工具，非常容易让人产生困扰。本章对OpenHarmony的构建体系进行了详细介绍，可以帮助开发者理清头绪，了解OpenHarmony的系统构建基础知识。

- 第5章，“启动流程”：本章详细分析了OpenHarmony的系统服务层中各大功能组件的详细启动流程。

- 第6章，“子系统”：本章分析了在开发OpenHarmony设备驱动时需要关注的部分子系统（特别是DFX子系统和IoT硬件子系统）的具体实现机制。

- 第7章，“分布式任务调度子系统”：本章详细分析了OpenHarmony系统服务框架的基础理念和实现机制，即所有功能和特性，都抽象为服务进行管理和使用。本章内容，在2021年下半年举行的第四届中国软件开源创新大赛代码评注组的评比中荣获二等奖。

- 第8章，“分布式通信子系统”：初步分析了分布式通信子系统的部分组件，其中的软总线组件是OpenHarmony实现万物互联/万物智联的基石。

- 第9章，“驱动子系统”：本章采用自下而上的方式深入分析了OpenHarmony驱动子系统的大量实现细节，为驱动开发者深入理解OpenHarmony的驱动框架提供了一个详细的参考。

受限于时间以及精力，本书暂未涉及与OpenHarmony系统移植相关的内容。但在本书交稿之后，我就开始尝试将OpenHarmony LTS 3.0系统移植到Raspberry Pi 4B开发板上，并将移植过程写成博文发布在个人的技术专栏上，请对系统移植感兴趣的读者到我的技术专栏去进一步学习参考。

另外，我其实也是OpenHarmony的初学者，因此本书中难免会有疏漏和错误之处，恳请各位读者在阅读本书时，如果产生疑问或发现错误，能积极与我联系，大家一起讨论，共同进步。



## 本书的特色

与市面上已有的偏向于HarmonyOS/OpenHarmony应用开发的图书不同，本书侧重于OpenHarmony系统分析和设备驱动开发，是一本深度剖析OpenHarmony运行机制的图书。

为了帮助读者更好地理解图书中的内容，本书从OpenHarmony的开发环境搭建、编译流程、系统启动流程等相对比较基础部分开始，逐渐深入到代码层面，对OpenHarmony的核心模块和框架的实现，展开细致入微的分析和解读。书中提供了大量的表格、详尽的日志、细致的程序流程图和数据结构关系图，以辅助读者理解相关内容，极大降低了学习的门槛。

本书可以为开发者深入地理解OpenHarmony的几个关键技术和驱动框架，提供一个非常好的帮助和参考。



## 本书的读者对象

本书面向广大的系统开发工程师、设备驱动开发工程师、高校软件相关专业学生以及对OpenHarmony技术感兴趣的从业者。

为了更好地阅读本书，读者最好具备下面提到的这些基本能力和经验知识。

- #### 熟悉C/C++语言

由于本书所分析的OpenHarmony子系统全都是使用C/C++语言实现的，并且会涉及到C/C++的一些非常抽象的知识，因此，如果没有足够的C/C++编程基础，则理解起来可能会比较困难。

- #### 熟悉Linux系统

对于驱动开发工程师来说，必须要熟悉Linux系统，并能做到熟练运用。本书在介绍OpenHarmony的具体实现，分析某些组件的运行流程时，会用到Linux系统中的一些命令和常规操作，读者需要对此有相应的了解。

- #### 了解操作系统的架构

本书的主旨是以解读源码的方式分析OpenHarmony系统的架构和具体的实现，虽然只涉及OpenHarmony的一小部分，但这部分也是OpenHarmony的核心。如果读者了解操作系统（特别是分布式操作系统）的架构，并具有一定的相关理论知识，则在学习本书时会有事半功倍的效果。即使读者不具备相关的知识，相信在学完本书后，也会对操作系统有一个比较具体完整的认识。

- #### 有一定的设备驱动开发经验

由于本书第9章是对OpenHarmony的驱动框架和驱动开发要点进行的深入分析，这是本书的重点内容，也是难点内容。如果没有相关的设备驱动开发经验，则在阅读这部分内容时会比较吃力。因此，为了更好地学习本书尤其是第9章的内容，读者最好具有一定的设备驱动开发经验。

 

## 如何阅读本书

本书第4章~第7章的大部分内容是以OpenHarmony v1.1.0 LTS（2021-04-01）版本的代码为基础写作完成的，在后期整理时，我对这些内容进行了更新，使其与OpenHarmony v3.0 LTS（2021-09-30）版本的代码同步。不过为了保证内容的简洁性和完整性，有少量内容仍然是以OpenHarmony v1.1.0 LTS版本的代码为基础，尽管这些内容没有做到与时俱进，但是其背后的理念仍然适用于OpenHarmony v3.0 LTS以及更新的版本。

第8章和第9章是以OpenHarmony v3.0 LTS(2021-09-30)版本的代码为基础写作完成的。为了保证本地代码的稳定性和一致性，我将这个版本的代码下载到本地后就没再与码云上的OpenHarmony v3.0 LTS分支的代码进行过同步。建议读者结合OpenHarmony v3.0 LTS版本的代码来学习这两章的内容，如果要在更新的版本上进行学习和验证，则需要读者自行对差异之处进行确认和分析。

因为OpenHarmony代码的迭代更新速度非常快，截至本书交稿时，Master分支的代码，相对于OpenHarmony v3.0 LTS版本的代码来说，又有了很大的变化。当书中内容涉及的代码结构因为版本变化而导致在Master分支上有较大调整时，会对此单独说明，请读者注意区分。

由于OpenHarmony的系统框架（第7章）和驱动子系统（第9章），大量采用了C语言来实现面向对象的编程，读者需要对C语言（特别是指针）和面向对象编程思想有足够的理解，这将会对理解本书的内容大有帮助。

另外，本书对OpenHarmony部分子系统的代码几乎做了API级别的分析，这会涉及非常详细的流程步骤和相应的解释。为了清晰地展示程序的执行步骤和过程，书中使用了大量的流程图、数据结构关系图和日志以辅助读者的理解。读者在阅读本书内容时，建议结合相关的流程图或数据结构关系图进行理解，效果会更好。



##  致谢

在本书写作过程中，得到了51CTO开源基础软件社区的大力支持，在此向社区的负责人王雪燕和杨文浩两位老师表示感谢。

同时感谢李传钊、祝尚元、连志安、刘果、欧建深等诸位专家，他们为本书提供了大量的修改意见和建议，保证了本书的技术深度和写作质量。

感谢人民邮电出版社的傅道坤编辑，为本书的整理、审校、出版做了非常多细致工作。

最重要的是要感谢华为的工程师，感谢他们的奋发图强和锐意开拓，为大家提供了这样一个充满自信和希望的操作系统。



## 本书的在线资源

本书提供了如下资源来帮助读者更好地学习理解本书的内容。

- #### 百度网盘

按本书2.3节的操作步骤搭建起来的两个开发环境，见百度网盘链接：

```
链接：https://pan.baidu.com/s/1KvSGFBf6pdyqdJTTq_1O6A?pwd=ohos
提取码：ohos
```

请阅读该目录下的README文档，根据需要选用其中之一即可。

- #### 码云资源仓库

与本书相关的示例代码、日志文档、流程图、表和说明文档等资源，均托管在码云（gitee）上，其链接为：

```
https://gitee.com/liangkzgitee/ohos_study_note
```

- #### 博客专栏文章

51CTO与华为HarmonyOS官方共建的开源基础软件社区，是开发者学习和讨论OpenHarmony的一个很好的平台。我在社区上开设了“鸿蒙系统学习笔记”技术专栏，其链接为：

```
https://ost.51cto.com/column/46
```

本书的勘误或未来我对OpenHarmony的学习总结，会在这个专栏上持续更新，敬请关注。

- #### 微信公众号

欢迎关注微信公众号：**鸿蒙系统学习笔记**（**liangkz-ohos**）

<img src="figures/liangkz-ohos.jpg" alt="liangkz-ohos" style="zoom:70%;" /> 

