# OHOS HDF 图谱-1-驱动配置信息树状图
梁开祝 2022.05.02

> 【**说明：**“OHOS HDF 图谱”系列文章，是《沉浸式剖析OpenHarmony源代码》一书第9章内容的补充材料。本系列文章将会给出大量的高清图片，要么是无法印刷到书里的超级大图，要么是书籍定稿后新近整理的图片。书籍中如已经有对图片的详细解释，本系列文章将只做简单介绍；如书中没有详细解释，本系列文章将补充详细说明。】
>
> **系列文章列表：**
> 《[与OpenHarmony共同成长：一年的历程和成果展示](https://ost.51cto.com/posts/10799)》



驱动配置信息是OHOS设备驱动程序的重要组成部分。

驱动配置信息的管理，以HCS（HDF Configuration Source）文件的形式组织源代码，以Key-Value形式对设备驱动信息进行配置和记录，实现了配置信息与驱动代码的解耦。
		HCS源代码被HC-GEN（HDF Configuration Generator）工具编译成HCB（HDF Configuration Binary）二进制文件。其中部署在内核态的hdf_hcs.hcb，会被加上ELF文件头、尾后重新封装为hdf_hcs_hex.o，链接进内核镜像中；而部署在用户态的hdf_default.hcb，则直接以hdf_default.hcb文件的形式部署在/system/etc/hdfconfig/ 或/vendor/etc/hdfconfig/ 目录下。
		在驱动框架启动阶段，hdf_hcs_hex.o或hdf_default.hcb被HCS Parser工具加载和分析，还原成以g_hcsTreeRoot为根节点的树状结构，最终被驱动框架各模块所使用。

我在《沉浸式剖析OpenHarmony源代码》一书中，基于LTS3.0版本代码的hispark_taurus_linux项目，对驱动配置信息做了详细的分析和整理，写了个测试函数，把g_hcsTreeRoot树状结构以文本的形式打印出来，大概的形式如下：

```
===========================Dbg_ParseHCStree:===========================
[r]-root,[n]-node,[a]-attr,[p]-parent,[c]-child,[s]-sibling,[++]-items,[--]-NULL
[r]:root
  0[n]:NodeName[root]  // root节点在level 0
  0[a]:++                // root 节点有属性字段，未展开
  0[p]:--                // root 节点无父节点 
  0[c]:platform         // root 节点的直接子节点是 platform节点
    1[n]:NodeName[platform]  // platform节点在 level 1
    1[a]:--                     // platform节点没有属性字段
    1[p]:root                   // platform节点的父节点是root节点
    1[c]:gpio_config          // platform节点的直接子节点是gpio_config节点
      2[n]:NodeName[gpio_config]   //gpio_config节点在 level 2
      2[a]:--                          //gpio_config节点没有属性字段
      2[p]:platform                   //gpio_config节点的父节点是platform节点
      2[c]:controller_0x120d0000   //gpio_config节点的直接子节点是controller_0x120d0000节点
        3[n]:NodeName[controller_0x120d0000]  //controller_0x120d0000节点在 level 3
         [A]:irqShare[0]                          //[A]是controller_0x120d0000节点的属性字段和值
         [A]:irqStart[48]
         [A]:regStep[0x1000]
         [A]:regBase[0x120D0000]
         [A]:bitNum[8]
         [A]:groupNum[12]
         [A]:match_attr[hisilicon_hi35xx_pl061]
        3[p]:gpio_config     //controller_0x120d0000节点的父节点是gpio_config节点
        3[c]:--                //controller_0x120d0000节点是叶子节点，没有子节点
        3[s]:--                //controller_0x120d0000节点是单独一片叶子节点，没有兄弟节点
      2[s]:i2c_config        //gpio_config节点的兄弟节点是i2c_config节点
  ------
   【此处略去若干中间节点】
  ------
    1[s]:device_info          // platform节点的兄弟节点是device_info节点
  ------
    1[n]:NodeName[device_info]  //device_info节点在 level 1
    1[a]:++                         //device_info节点有属性字段，未展开
    1[p]:root                       //device_info节点的父节点是root节点
    1[c]:platform                  //device_info节点的直接子节点是level 2的platform host节点，
                                       // 不是上面level 1的那个platform节点
  ------
   【此处略去若干中间节点】
  ------
    1[n]:NodeName[uart_test] //uart_test节点在 level 1，与platform/device_info节点是兄弟关系
    1[a]:++                      //uart_test节点有属性字段，未展开
    1[p]:root                   //uart_test节点的父节点是 root 节点
    1[c]:--                      //uart_test节点无子节点
    1[s]:--                      //uart_test节点无兄弟节点
  0[s]:--        //root节点level 0，无兄弟节点
[r]:Node Num[213], Max Level[6]  //总共213个节点，最大层级7层[0~6]
===========================Dbg_ParseHCStree.===========================
```
我还将上面的结构，整理到了一张图上，非常直观地展示了g_hcsTreeRoot树状结构中各个节点（包括Host）之间的层级关系和deviceMatchAttr的匹配关系，如下图：
![LTS3.0_HdfHcs20220331_signed.png](figures/1-LTS3.0_HdfHcs20220331_signed.png)

上文提到的函数源代码、解析出来的树状结构完整文本和原始大图，都放在附件(见当前目录下)了，感兴趣的小伙伴请自行下载回去理解一下。

